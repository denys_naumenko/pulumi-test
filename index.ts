import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";

const cluster = new awsx.ecs.Cluster("cluster")

const lb = new awsx.elasticloadbalancingv2.ApplicationLoadBalancer(
    "net-lb", {external: true, securityGroups: cluster.securityGroups}
)

const web = lb.createListener("web", {port: 80, external: true})

const img = awsx.ecs.Image.fromPath("app-img", "./app")

const appServer = new awsx.ecs.FargateService("app-srv",
{
    cluster: cluster,
    taskDefinitionArgs: {
        container: {
            image: img,
            cpu: 128,
            memory: 150,
            portMappings: [web],
        }
    },
    desiredCount: 1,
}
)

export const url = web.endpoint.hostname